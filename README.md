# **PixelInterface** ☝️

<img align="right" src="icon.png">

Pixel-perfect fonts and user interface elements.

- 📦 <http://henrysoftware.itch.io/godot-pixel-interface>
- 🌐 <http://rakkarage.github.io/PixelInterface>
- 📃 <http://guthub.com/rakkarage/PixelInterface>

[![.github/workflows/compress.yml](https://github.com/rakkarage/PixelInterface/actions/workflows/compress.yml/badge.svg)](https://github.com/rakkarage/PixelInterface/actions/workflows/compress.yml)
[![.github/workflows/deploy.yml](https://github.com/rakkarage/PixelInterface/actions/workflows/deploy.yml/badge.svg)](https://github.com/rakkarage/PixelInterface/actions/workflows/deploy.yml)
